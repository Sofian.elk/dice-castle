import java.util.Random;

public class Dice {
    
    private static final int minValue = 1;
    private static final int maxValue = 6;

    public int throwDice(){
        Random r = new Random();
        return r.nextInt((maxValue - minValue)+1) + minValue;
    }

}
