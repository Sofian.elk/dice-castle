import java.util.Scanner;

public class Main {
    public static void main (String[] args){


        System.out.println("Hello World");

        boolean gameOver = false;
        Scanner scanner = new Scanner(System.in);

        while(gameOver == false){

            System.out.print("Ecrire un nombre: ");

            int nombre = scanner.nextInt();

            System.out.println(nombre);

            if (nombre == 10){
                System.out.println("Merci d'avoir participé à notre jeu");
                gameOver = true;
            }

        }
        scanner.close();
        
    }
}
