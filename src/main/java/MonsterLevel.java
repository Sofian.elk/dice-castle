
public class MonsterLevel {

    private int lvl;
    private int[] damageRange;
    private int damage;

    public static int getMonsterDamage(int lvl) {

        switch (lvl) {
        case 1:
        case 2:
            return 1;
        case 3:
            return 2;
        }

        return 0;
    }

    public static int[] getMonsterDamageRange(int lvl) {

        switch (lvl) {
        case 1:
            return new int[] { 1, 2 };
        case 2:
        case 3:
            return new int[] { 1, 2, 3 };
        }

        return new int[] { 0 };

    }
}
