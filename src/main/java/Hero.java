public class Hero {

    public int hp;
    public final static int hpMax = 5;
    private int location;
    private boolean hasSword;
    private Dice dice;

    public Hero(Dice dice) {
        location = 0;
        hasSword = false;
        this.dice = dice;
        hp = 3;
    }

    public int getLocation() {

        return location;
    }

    public void moveForward() {
        location += dice.throwDice();
        if (location < Castle.nbCells) {
            return;
        }

        location = Castle.nbCells;
    }

    public boolean sword() {

        hasSword = (location == Castle.nbCells);
        return hasSword;
    }

    public boolean endGame() {

        return hasSword;
    }

    public void setHp(int newHp) {
        hp += newHp;
        if (hp > hpMax) {
            hp = hpMax;
        }
    }

    public void fightMonster(Monster monster) {

        int diceValue = dice.throwDice();
        int damage = monster.getDamage();
        int[] damageRange = monster.getDamageRange();

        for (int i = 0; i <= damageRange.length - 1; i++) {
            if (diceValue == damageRange[i]) {
                hp -= damage;
            }

        }

    }

}