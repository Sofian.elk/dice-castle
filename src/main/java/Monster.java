public class Monster {
    private String name;
    private int lvl;
    private int location;
    private int damage;
    private int[] damageRange;
    

    public Monster(String name, int lvl, int location){
        this.name = name;
        this.lvl = lvl;
        this.location = location;
        damage = MonsterLevel.getMonsterDamage(this.lvl);
        damageRange = MonsterLevel.getMonsterDamageRange(this.lvl);
    }   

    public int getLocation(){
        return location;
    }
    public String getName(){
        return name;
    }
    public int[] getDamageRange(){
        return damageRange;
    }
    public int getDamage(){
        return damage;
    }
}