public class Potion {
    
    private int location;
    private final int healNbHp;

    public Potion(int location){
        this.location = location;
        this.healNbHp = 1;
    }   
    public int getLocation(){
        return location;
    }
}
