import java.util.List;
import java.util.Arrays;

public class DiceCastle {
    private boolean gameOver;
    private Hero hero;
    private List<Monster> monsters;
    private List<Potion> potions;

    public DiceCastle(Hero hero) {
        gameOver = false;
        
        this.hero = hero;
        
        Monster bat = new Monster("Chauve-souris", 1, 2);
        Monster zombie = new Monster("Zombie", 1, 5);
        Monster spider = new Monster("Araignée", 2, 15);
        Monster skullKing = new Monster("Roi squelette", 3, 16);
        
        monsters = Arrays.asList(bat, zombie, spider, skullKing);
        
        Potion firstPotion = new Potion(13);
        Potion secondPotion = new Potion(13);
        
        potions = Arrays.asList(firstPotion, secondPotion);
        
    }
    
    public boolean getGameOver() {
        return gameOver;
    }
    
    public void setGameOver(boolean isGameOver) {
        gameOver = isGameOver;
    }
    
    public List<Monster> getMonsters() {
        return monsters;
    }
    public List<Potion> getPotions() {
        return potions;
    }
    
    public void checkGameOver() {
        if (hero.hp <= 0) {
            setGameOver(true);
        }
    }
    
    public Monster checkMonsterEncounter() {

        Monster monster;

        for (int i = 0; i < monsters.size(); i++) {
            monster = monsters.get(i);
            if (hero.getLocation() == monster.getLocation())
                return monsters.get(i);
        }

        return null;
    }
    public Potion checkPotionEncounter() {

        Potion potion;

        for (int i = 0; i < potions.size(); i++) {
            potion = potions.get(i);
            if (hero.getLocation() == potion.getLocation())
                return potions.get(i);
        }

        return null;
    }

}
