import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DiceCastleTest {

    private DiceCastle game;
    private Hero hero;

    private DiceCastle pipedGame;
    private Hero pipedHero;

    @BeforeEach
    public void setup() {
        hero = new Hero(new Dice());
        game = new DiceCastle(hero);

        /*---------------piped game-------------*/
        pipedHero = new Hero((new DicePiped()));
        pipedGame = new DiceCastle(pipedHero);
    }

    @Test
    public void HeroMoveForward() {
        // GIVEN setup

        // WHEN
        hero.moveForward();

        // THEN
        assertTrue(hero.getLocation() >= 1 && hero.getLocation() <= 6);
    }

    @Test
    public void MoveForwardMaxCell() {
        // GIVEN setup

        // WHEN
        for (int i = 0; i < Castle.nbCells; i++) {
            hero.moveForward();
        }

        // THEN
        assertEquals(Castle.nbCells, hero.getLocation());
    }

    @Test
    public void GameOverSword() {
        // GIVEN setup

        // WHEN
        for (int i = 0; i < Castle.nbCells; i++) {
            hero.moveForward();
        }
        boolean result = hero.sword();

        // THEN
        assertEquals(true, result);
    }

    @Test
    public void GameOverDead() {
        // GIVEN setup

        // WHEN
        hero.setHp(-Hero.hpMax);
        game.checkGameOver();

        // THEN
        assertEquals(true, game.getGameOver());
    }

    @Test
    public void diceResult() {
        // GIVEN
        Dice dice = new Dice();
        // WHEN
        int result = dice.throwDice();
        // THEN
        assertAll(() -> assertTrue(result >= 1, "Test Minimum Value"),
                () -> assertTrue(result <= 6, "Test Maximum Value"));
    }

    @Test
    public void CheckMaxHp() {
        // GIVEN setup

        // WHEN
        hero.setHp(4);

        // THEN
        assertEquals(5, hero.hp);
    }

    @Test
    public void CheckPipedDice() {
        // GIVEN setup

        // WHEN
        pipedHero.moveForward();

        // THEN
        assertEquals(1, pipedHero.getLocation());
    }

    @Test
    public void movePipedHero() {
        // GIVEN setup

        // WHEN
        pipedHero.moveForward();

        // THEN
        assertEquals(1, pipedHero.getLocation());
    }

    @Test
    public void monsterEncounter() {
        // GIVEN setup

        // WHEN
        pipedHero.moveForward();
        pipedHero.moveForward();
        Monster monster = pipedGame.checkMonsterEncounter();

        // THEN
        assertEquals(pipedGame.getMonsters().get(0), monster);
    }

    @Test
    public void potionEncounter() {
        // GIVEN setup

        // WHEN
        for (int i = 1; i <= 13; i++) {
            pipedHero.moveForward();
        }

        Potion potion = pipedGame.checkPotionEncounter();

        // THEN
        assertEquals(pipedGame.getPotions().get(0), potion);
    }

    @Test
    public void TestFightMonster1() {
        // GIVEN setup

        // WHEN
        for (int i = 1; i <= 2; i++) {
            pipedHero.moveForward();
        }
        Monster monster = pipedGame.checkMonsterEncounter();

        if (Objects.nonNull(monster)) {
            pipedHero.fightMonster(monster);
        }

        // THEN
        assertEquals(2, pipedHero.hp);
    }

    @Test
    public void TestFightMonster2() {
        // GIVEN setup

        // WHEN
        for (int i = 1; i <= 15; i++) {
            pipedHero.moveForward();
        }
        Monster monster = pipedGame.checkMonsterEncounter();

        if (Objects.nonNull(monster)) {
            pipedHero.fightMonster(monster);
        }

        // THEN
        assertEquals(2, pipedHero.hp);
    }

    @Test
    public void TestFightMonster3() {
        // GIVEN setup

        // WHEN
        for (int i = 1; i <= 16; i++) {
            pipedHero.moveForward();
        }
        Monster monster = pipedGame.checkMonsterEncounter();


        if (Objects.nonNull(monster)) {
            pipedHero.fightMonster(monster);
        }

        // THEN
        assertEquals(1, pipedHero.hp);
    }

}
